#!/bin/sh

set -e

for file in *.in.h ; do
    echo "Preprocessing $file"
    ./preprocess_header.c $file `basename $file .in.h`.h
done
