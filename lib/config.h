#define _GL_INLINE_HEADER_BEGIN
#define _GL_INLINE_HEADER_END
#define _GL_ATTRIBUTE_MALLOC
#define _GL_ATTRIBUTE_PURE
#define _GL_EXTERN_INLINE extern inline
#define _GL_INLINE static inline
#define _GL_ASYNC_SAFE
#define _GL_CXXALIAS_SYS(x, y, z)
#define _GL_UNUSED
#define _GL_ATTRIBUTE_FORMAT_PRINTF(x, y)
#define _GL_ARG_NONNULL(x)
#define _GNU_SOURCE
#define PACKAGE_BUGREPORT "PACKAGE_BUGREPORT"
#define PACKAGE_NAME "PACKAGE_NAME"
#define PACKAGE "PACKAGE"
#define GNULIB_STRERROR_R_POSIX 1
#define GNULIB_CLOSE_STREAM 1
#define HAVE___FREADAHEAD 1
#define HAVE_DECL_PROGRAM_INVOCATION_SHORT_NAME 1
