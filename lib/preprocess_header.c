#!/bin/tcc -run

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>

void replace_placeholder(const char *ph, FILE *fout) {
    if (strcmp(ph, "PRAGMA_SYSTEM_HEADER") == 0) return;
    if (strcmp(ph, "PRAGMA_COLUMNS") == 0) return;
    if (strcmp(ph, "GUARD_PREFIX") == 0) return;
    if (strcmp(ph, "INCLUDE_NEXT") == 0) { fprintf(fout, "include_next"); return; }
    if (strcmp(ph, "INCLUDE_NEXT_AS_FIRST_DIRECTIVE") == 0) { fprintf(fout, "include_next"); return; }
    if (strcmp(ph, "EMULTIHOP_HIDDEN") == 0) { fprintf(fout, "0"); return; }
    if (strcmp(ph, "EMULTIHOP_VALUE") == 0) { fprintf(fout, "EMULTIHOP"); return; }
    if (strcmp(ph, "ENOLINK_HIDDEN") == 0) { fprintf(fout, "0"); return; }
    if (strcmp(ph, "ENOLINK_VALUE") == 0) { fprintf(fout, "ENOLINK"); return; }
    if (strcmp(ph, "EOVERFLOW_HIDDEN") == 0) { fprintf(fout, "0"); return; }
    if (strcmp(ph, "EOVERFLOW_VALUE") == 0) { fprintf(fout, "EOVERFLOW"); return; }
    if (strcmp(ph, "ICONV_CONST") == 0) { fprintf(fout, "const"); return; }
    if (strcmp(ph, "PRI_MACROS_BROKEN") == 0) { fprintf(fout, "0"); return; }
    if (strcmp(ph, "APPLE_UNIVERSAL_BUILD") == 0) { fprintf(fout, "0"); return; }
    if (strcmp(ph, "INT64_MAX_EQ_LONG_MAX") == 0) { fprintf(fout, "0"); return; }
    if (strcmp(ph, "UINT64_MAX_EQ_ULONG_MAX") == 0) { fprintf(fout, "0"); return; }
    if (strcmp(ph, "INT32_MAX_LT_INTMAX_MAX") == 0) { fprintf(fout, "0"); return; }
    if (strcmp(ph, "UINT32_MAX_LT_UINTMAX_MAX") == 0) { fprintf(fout, "0"); return; }
    if (strcmp(ph, "BITSIZEOF_PTRDIFF_T") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "PTRDIFF_T_SUFFIX") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "BITSIZEOF_SIG_ATOMIC_T") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "SIG_ATOMIC_T_SUFFIX") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "BITSIZEOF_SIZE_T") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "SIZE_T_SUFFIX") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "BITSIZEOF_WCHAR_T") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "WCHAR_T_SUFFIX") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "BITSIZEOF_WINT_T") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "WINT_T_SUFFIX") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "PRIPTR_PREFIX") == 0) return;
    if (strcmp(ph, "ASM_SYMBOL_PREFIX") == 0) { fprintf(fout, "\"\""); return; }
    if (strcmp(ph, "UNDEFINE_STRTOK_R") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "SYS_IOCTL_H_HAVE_WINSOCK2_H") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "SYS_IOCTL_H_HAVE_WINSOCK2_H_AND_USE_SOCKETS") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "WINDOWS_64_BIT_ST_SIZE") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "WINDOWS_64_BIT_OFF_T") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "WINDOWS_STAT_TIMESPEC") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "WINDOWS_STAT_INODES") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "BROKEN_THRD_START_T") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "TIME_H_DEFINES_STRUCT_TIMESPEC") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "SYS_TIME_H_DEFINES_STRUCT_TIMESPEC") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "PTHREAD_H_DEFINES_STRUCT_TIMESPEC") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "UNISTD_H_DEFINES_STRUCT_TIMESPEC") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "UNISTD_H_HAVE_WINSOCK2_H") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "UNISTD_H_HAVE_WINSOCK2_H_AND_USE_SOCKETS") == 0) { fprintf(fout, "unused"); return; }
    if (strcmp(ph, "HAVE_DECL_ENVIRON") == 0) { fprintf(fout, "0"); return; }
    if (strcmp(ph, "HAVE_SYS_CDEFS_H") == 0) { fprintf(fout, "0"); return; }
    if (strcmp(ph, "HAVE_XLOCALE_H") == 0) { fprintf(fout, "0"); return; }
    if (strncmp(ph, "HAVE_", 5) == 0) { fprintf(fout, "1"); return; }
    if (strcmp(ph, "GNULIB_ENVIRON") == 0) { fprintf(fout, "1"); return; }
    if (strcmp(ph, "GNULIB_CLOSE_STREAM") == 0) { fprintf(fout, "1"); return; }
    if (strncmp(ph, "GNULIB_", 7) == 0) { fprintf(fout, "0"); return; }
    if (strncmp(ph, "REPLACE_", 8) == 0) { fprintf(fout, "0"); return; }
    if (strcmp(ph, "NEXT_AS_FIRST_DIRECTIVE_MATH_H") == 0) { fprintf(fout, "<math.h>"); return; }
    if (strncmp(ph, "NEXT_", 5) == 0) {
        char tmp[1024];
        strcpy(tmp, ph+5);
        for (char *p = tmp; *p != 0; p++) {
            *p = (*p != '_') ? tolower(*p) : '.';
        }
        fprintf(fout, "<%s>", tmp);
        return;
    }
    fprintf(stderr, "Unknown placeholder %s\n", ph);
    exit(1);
}

int main(int argc, char *argv[]) {
    assert(argc == 3);
    FILE *fin = fopen(argv[1], "r");
    FILE *fout = fopen(argv[2], "w");
    int c;
    while ((c = fgetc(fin)) != EOF) {
        if (c != '@') {
            fputc(c, fout);
        } else {
            char buf[1024];
            char *pos = buf;
            bool good = true;
            while ((c = getc(fin)) != '@') {
                assert(c != EOF);
                assert(pos -  buf < 1024);
                *pos++ = c;
                if (islower(c)) {
                    good = false;
                    break;
                }
            }
            *pos = 0;
            if (good) {
                replace_placeholder(buf, fout);
            } else {
                fprintf(fout, "@%s", buf);
            }
        }
    }
    fclose(fin);
    fclose(fout);
    return 0;
}
